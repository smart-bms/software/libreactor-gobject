/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/**
 * SECTION:reactor-polynomial
 * @short_description: reactor polynomial
 * @title:ReactorPolynomial
 *
 * #ReactorPolynomial implements serializable object that represents
 * a third degree polynomial used to define aproximation of some functions.
 * Internally it uses fixed-point arithmetic computation.
 */

#include "reactor-polynomial.h"

G_DEFINE_BOXED_TYPE (ReactorPolynomial, reactor_polynomial,
		     reactor_polynomial_copy,
		     reactor_polynomial_free);

/**
 * reactor_polynomial_new:
 *
 * Creates empty polynomial.
 *
 * returns: (transfer full): new #ReactorPolynomial
 */
ReactorPolynomial *
reactor_polynomial_new ()
{
    return reactor_polynomial_new_from_poly (NULL);
}

/**
 * reactor_polynomial_new_from_poly: (skip)
 * @poly: internal instance
 *
 * Creates polynomial for @poly.
 *
 * returns: (transfer full): new #ReactorPolynomial
 */
ReactorPolynomial *
reactor_polynomial_new_from_poly (struct rcom_poly *poly)
{
    gfloat coeffs[RCOM_POLY_DEGREE + 1];
    gint i;

    if (poly != NULL) {
	for (i = 0; i <= RCOM_POLY_DEGREE; i++) {
	    coeffs[i] = rcom_poly_coeff_float (poly, i);
	}
    } else {
	for (i = 0; i <= RCOM_POLY_DEGREE; i++) {
	    coeffs[i] = 0;
	}
    }

    return reactor_polynomial_new_from_array (coeffs, G_N_ELEMENTS (coeffs));
}

/**
 * reactor_polynomial_new_from_array:
 * @coeffs: (array length=count): array of coefficients
 * @count: number of coefficients
 *
 * Creates new polynomial for given floating point coefficients.
 * First element of an array is the x^0 coefficient, second is the x^1
 * coefficient and so. If the array is shorter than the degree, the last
 * coefficients are set to 0. If it's longer than the maximum degree, the first
 * elements are taken with a warning message.
 *
 * returns: (transfer full) (nullable): new #ReactorPolynomial
 */
ReactorPolynomial *
reactor_polynomial_new_from_array (const gfloat *coeffs,
				   gint count)
{
    ReactorPolynomial *self;
    gint i;

    if (count > RCOM_POLY_DEGREE + 1) {
	g_warning ("trying to create polynomial of higher degree than %d: %d",
		   RCOM_POLY_DEGREE, count - 1);
    }

    self = g_slice_new (ReactorPolynomial);

    for (i = 0; i < MIN (RCOM_POLY_DEGREE + 1, count); i++) {
	self->coeffs[i] = coeffs[i];
    }

    for (i = count; i < RCOM_POLY_DEGREE + 1; i++) {
	self->coeffs[i] = 0;
    }

    return self;
}

/**
 * reactor_polynomial_copy:
 * @self: a #ReactorPolynomial
 *
 * Copies @self.
 * 
 * returns: (transfer full): copied @self
 */
ReactorPolynomial *
reactor_polynomial_copy (ReactorPolynomial *self)
{
    return g_slice_dup (ReactorPolynomial, self);
}

/**
 * reactor_polynomial_free:
 * @self: a #ReactorPolynomial
 *
 * Frees @self.
 */
void
reactor_polynomial_free (ReactorPolynomial *self)
{
    g_slice_free (ReactorPolynomial, self);
}

/**
 * reactor_polynomial_init_poly: (skip)
 * @self: a #ReactorPolynomial
 * @poly: internal object
 *
 * Initializes @self with @poly fields.
 */
void
reactor_polynomial_init_poly (ReactorPolynomial *self,
			      struct rcom_poly *poly)
{
    rcom_poly_init_float (poly, self->coeffs);
}

/**
 * reactor_polynomial_compute:
 * @self: a #ReactorPolynomial
 * @value: integer value to pass for computation
 *
 * Computes polynomial output for given @value.
 *
 * returns: the result
 */
guint32
reactor_polynomial_compute (ReactorPolynomial *self,
			    guint32 value)
{
    struct rcom_poly poly;

    reactor_polynomial_init_poly (self, &poly);

    return rcom_poly_compute (&poly, value);
}
