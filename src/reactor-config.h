/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <glib-object.h>
#include <rcom.h>

#define REACTOR_TYPE_CONFIG (reactor_config_get_type ())

G_DECLARE_FINAL_TYPE (ReactorConfig, reactor_config,
		      REACTOR, CONFIG, GObject);

ReactorConfig *reactor_config_new ();
ReactorConfig *reactor_config_new_copy (ReactorConfig *other);
ReactorConfig *reactor_config_new_with_config (const struct rcom_conf *config);
struct rcom_conf *reactor_config_get_config (ReactorConfig *self);
