/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include "reactor-config.h"
#include "reactor-types.h"

G_BEGIN_DECLS

#define REACTOR_COM_ERROR (reactor_com_error_quark ())

enum
{
    REACTOR_COM_ERROR_SEND,
    REACTOR_COM_ERROR_OPEN,
};

GQuark reactor_com_error_quark () G_GNUC_CONST;

#define REACTOR_TYPE_COM (reactor_com_get_type ())

G_DECLARE_DERIVABLE_TYPE (ReactorCom, reactor_com,
			  REACTOR, COM, GObject);

struct _ReactorComClass
{
    GObjectClass parent_class;
};

gint8 reactor_com_send_ping (ReactorCom *self,
			     guint8 ack,
			     GError **error);
gint8 reactor_com_send_timestamp (ReactorCom *self,
				  gint64 timestamp,
				  GError **error);
gint8 reactor_com_send_message (ReactorCom *self,
				const gchar *message,
				GError **error);
gint8 reactor_com_send_voltage (ReactorCom *self,
				const gint16 *cells,
				guint8 count,
				GError **error);
gint8 reactor_com_send_current (ReactorCom *self,
				gint32 current,
				GError **error);
gint8 reactor_com_send_status (ReactorCom *self,
			       ReactorComStatus status,
			       GError **error);
gint8 reactor_com_send_protection (ReactorCom *self,
				   ReactorComProtection protection,
				   GError **error);
gint8 reactor_com_send_balancing (ReactorCom *self,
				  guint32 balancing,
				  GError **error);
gint8 reactor_com_send_temperature (ReactorCom *self,
				    const gint16 *temperatures,
				    guint8 count,
				    GError **error);
gint8 reactor_com_send_config (ReactorCom *self,
			       ReactorConfig *config,
			       GError **error);
gint8 reactor_com_send_caplimit (ReactorCom *self,
				 guint8 limit,
				 GError **error);
gint8 reactor_com_send_query (ReactorCom *self,
			      ReactorComQuery query,
			      GError **error);
gint8 reactor_com_send_set (ReactorCom *self,
			    ReactorComStatus status,
			    GError **error);
gint8 reactor_com_send_reset (ReactorCom *self,
			      ReactorComStatus status,
			      GError **error);
gint8 reactor_com_send_bq769x0 (ReactorCom *self,
				ReactorComBq769x0 action,
				guint8 address,
				gint size,
				const guint8 *data,
				GError **error);

G_END_DECLS
