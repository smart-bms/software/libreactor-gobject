/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/**
 * SECTION:reactor-config
 * @short_description: reactor config
 * @title:ReactorConfig
 *
 * #ReactorConfig represents Reactor BMS configuration structure that
 * defines behaviour of the device. It maps internal structure fields into
 * GObject properties.
 */

#include "reactor-config.h"
#include "reactor-polynomial.h"

struct _ReactorConfig
{
    GObject parent_instance;
    struct rcom_conf *config;
};

G_DEFINE_TYPE (ReactorConfig, reactor_config, G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_CONFIG,
    PROP_CELL_OV,
    PROP_CELL_OV_RELEASE,
    PROP_CELL_UV,
    PROP_CELL_UV_RELEASE,
    PROP_CHG_CURRENT_LIMIT,
    PROP_DSG_CURRENT_LIMIT,
    PROP_CHG_CURRENT_DELAY,
    PROP_DSG_CURRENT_DELAY,
    PROP_CHG_TEMPERATURE_MIN,
    PROP_CHG_TEMPERATURE_MAX,
    PROP_DSG_TEMPERATURE_MIN,
    PROP_DSG_TEMPERATURE_MAX,
    PROP_NOMINAL_CAPACITY,
    PROP_NOMINAL_VOLTAGE,
    PROP_CAPACITY_POLYNOMIAL,
    PROP_VITALITY_POLYNOMIAL,
    PROP_CAPACITY_LIMIT_MIN,
    PROP_CAPACITY_LIMIT_MAX,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void finalize (GObject *gobj);
static void set_property (GObject *gobj, guint propid,
			  const GValue *value,
			  GParamSpec *spec);
static void get_property (GObject *gobj, guint propid,
			  GValue *value,
			  GParamSpec *spec);
static void
reactor_config_init (ReactorConfig *self G_GNUC_UNUSED)
{
    
}

static void
reactor_config_class_init (ReactorConfigClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->finalize = finalize;
    gcls->set_property = set_property;
    gcls->get_property = get_property;

    props[PROP_CONFIG] =
	g_param_spec_pointer ("config",
			      "config",
			      "rcom config structure",
			      G_PARAM_READWRITE |
			      G_PARAM_CONSTRUCT_ONLY |
			      G_PARAM_STATIC_STRINGS);

    props[PROP_CELL_OV] =
	g_param_spec_int ("cell-ov",
			  "cell ov",
			  "cell overvoltage",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CELL_OV_RELEASE] =
	g_param_spec_int ("cell-ov-release",
			  "cell ov release",
			  "cell overvoltage release",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CELL_UV] =
	g_param_spec_int ("cell-uv",
			  "cell uv",
			  "cell undervoltage",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CELL_UV_RELEASE] =
	g_param_spec_int ("cell-uv-release",
			  "cell uv release",
			  "cell undervoltage release",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CHG_CURRENT_LIMIT] =
	g_param_spec_int ("chg-current-limit",
			  "chg current limit",
			  "charging current limit",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_DSG_CURRENT_LIMIT] =
	g_param_spec_int ("dsg-current-limit",
			  "dsg current limit",
			  "discharging current limit",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CHG_CURRENT_DELAY] =
	g_param_spec_int ("chg-current-delay",
			  "chg current delay",
			  "charging current delay",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_DSG_CURRENT_DELAY] =
	g_param_spec_int ("dsg-current-delay",
			  "dsg current delay",
			  "discharging current delay",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CHG_TEMPERATURE_MIN] =
	g_param_spec_int ("chg-temperature-min",
			  "chg temperature min",
			  "minimum charging temperature",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CHG_TEMPERATURE_MAX] =
	g_param_spec_int ("chg-temperature-max",
			  "chg temperature max",
			  "maximum charging temperature",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_DSG_TEMPERATURE_MIN] =
	g_param_spec_int ("dsg-temperature-min",
			  "dsg temperature min",
			  "minimum discharging temperature",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_DSG_TEMPERATURE_MAX] =
	g_param_spec_int ("dsg-temperature-max",
			  "dsg temperature max",
			  "maximum discharging temperature",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_NOMINAL_CAPACITY] =
	g_param_spec_int ("nominal-capacity",
			  "nominal capacity",
			  "nominal capacity",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_NOMINAL_VOLTAGE] =
	g_param_spec_int ("nominal-voltage",
			  "nominal voltage",
			  "nominal voltage",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CAPACITY_POLYNOMIAL] =
	g_param_spec_boxed ("capacity-polynomial",
			    "capacity polynomial",
			    "polynomial to compute capacity from voltage",
			    REACTOR_TYPE_POLYNOMIAL,
			    G_PARAM_READWRITE |
			    G_PARAM_STATIC_STRINGS);

    props[PROP_VITALITY_POLYNOMIAL] =
	g_param_spec_boxed ("vitality-polynomial",
			    "vitality polynomial",
			    "polynomial to compute vitality from capacity limit",
			    REACTOR_TYPE_POLYNOMIAL,
			    G_PARAM_READWRITE |
			    G_PARAM_STATIC_STRINGS);

    props[PROP_CAPACITY_LIMIT_MIN] =
	g_param_spec_int ("capacity-limit-min",
			  "capacity limit min",
			  "minimum capacity limit percentage value",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    props[PROP_CAPACITY_LIMIT_MAX] =
	g_param_spec_int ("capacity-limit-max",
			  "capacity limit max",
			  "maximum capacity limit percentage value",
			  0, G_MAXINT32, 0,
			  G_PARAM_READWRITE |
			  G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

static void
finalize (GObject *gobj)
{
    ReactorConfig *self;

    self = REACTOR_CONFIG (gobj);

    g_clear_pointer (&self->config, g_free);

    G_OBJECT_CLASS (reactor_config_parent_class)->finalize (gobj);
}

static void
set_property (GObject *gobj, guint propid,
	      const GValue *value,
	      GParamSpec *spec)
{
    ReactorConfig *self;
    ReactorPolynomial *p;
    struct rcom_poly poly;
    struct rcom_conf *c;

    self = REACTOR_CONFIG (gobj);
    c = reactor_config_get_config (self);

    switch (propid) {
    case PROP_CONFIG:
	g_clear_pointer (&self->config, g_free);
	self->config = g_memdup (g_value_get_pointer (value),
				 sizeof (struct rcom_conf));
	break;

    case PROP_CELL_OV:
	c->cell_ov = g_value_get_int (value);
	break;

    case PROP_CELL_OV_RELEASE:
	c->cell_ov_release = g_value_get_int (value);
	break;

    case PROP_CELL_UV:
	c->cell_uv = g_value_get_int (value);
	break;

    case PROP_CELL_UV_RELEASE:
	c->cell_uv_release = g_value_get_int (value);
	break;

    case PROP_CHG_CURRENT_LIMIT:
	c->chg_current_limit = g_value_get_int (value);
	break;

    case PROP_DSG_CURRENT_LIMIT:
	c->dsg_current_limit = g_value_get_int (value);
	break;

    case PROP_CHG_CURRENT_DELAY:
	c->chg_current_delay = g_value_get_int (value);
	break;

    case PROP_DSG_CURRENT_DELAY:
	c->dsg_current_delay = g_value_get_int (value);
	break;

    case PROP_CHG_TEMPERATURE_MIN:
	c->chg_temperature_min = g_value_get_int (value);
	break;

    case PROP_CHG_TEMPERATURE_MAX:
	c->chg_temperature_max = g_value_get_int (value);
	break;

    case PROP_DSG_TEMPERATURE_MIN:
	c->dsg_temperature_min = g_value_get_int (value);
	break;

    case PROP_DSG_TEMPERATURE_MAX:
	c->dsg_temperature_max = g_value_get_int (value);
	break;

    case PROP_NOMINAL_CAPACITY:
	c->nominal_capacity = g_value_get_int (value);
	break;

    case PROP_NOMINAL_VOLTAGE:
	c->nominal_voltage = g_value_get_int (value);
	break;

    case PROP_CAPACITY_POLYNOMIAL:
	p = g_value_get_boxed (value);
	reactor_polynomial_init_poly (p, &poly);
	c->capacity_polynomial = poly;
	break;

    case PROP_VITALITY_POLYNOMIAL:
	p = g_value_get_boxed (value);
	reactor_polynomial_init_poly (p, &poly);
	c->vitality_polynomial = poly;
	break;

    case PROP_CAPACITY_LIMIT_MIN:
	c->capacity_limit_min = g_value_get_int (value);
	break;

    case PROP_CAPACITY_LIMIT_MAX:
	c->capacity_limit_max = g_value_get_int (value);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
get_property (GObject *gobj, guint propid,
	      GValue *value,
	      GParamSpec *spec)
{
    ReactorPolynomial *p;
    struct rcom_poly poly;
    struct rcom_conf *c;

    c = reactor_config_get_config (REACTOR_CONFIG (gobj));

    switch (propid) {
    case PROP_CELL_OV:
	g_value_set_int (value, c->cell_ov);
	break;

    case PROP_CELL_OV_RELEASE:
	g_value_set_int (value, c->cell_ov_release);
	break;

    case PROP_CELL_UV:
	g_value_set_int (value, c->cell_uv);
	break;

    case PROP_CELL_UV_RELEASE:
	g_value_set_int (value, c->cell_uv_release);
	break;

    case PROP_CHG_CURRENT_LIMIT:
	g_value_set_int (value, c->chg_current_limit);
	break;

    case PROP_DSG_CURRENT_LIMIT:
	g_value_set_int (value, c->dsg_current_limit);
	break;

    case PROP_CHG_CURRENT_DELAY:
	g_value_set_int (value, c->chg_current_delay);
	break;

    case PROP_DSG_CURRENT_DELAY:
	g_value_set_int (value, c->dsg_current_delay);
	break;

    case PROP_CHG_TEMPERATURE_MIN:
	g_value_set_int (value, c->chg_temperature_min);
	break;

    case PROP_CHG_TEMPERATURE_MAX:
	g_value_set_int (value, c->chg_temperature_max);
	break;

    case PROP_DSG_TEMPERATURE_MIN:
	g_value_set_int (value, c->dsg_temperature_min);
	break;

    case PROP_DSG_TEMPERATURE_MAX:
	g_value_set_int (value, c->dsg_temperature_max);
	break;

    case PROP_NOMINAL_CAPACITY:
	g_value_set_int (value, c->nominal_capacity);
	break;

    case PROP_NOMINAL_VOLTAGE:
	g_value_set_int (value, c->nominal_voltage);
	break;

    case PROP_CAPACITY_POLYNOMIAL:
	poly = c->capacity_polynomial;
	p = reactor_polynomial_new_from_poly (&poly);
	g_value_take_boxed (value, p);
	break;

    case PROP_VITALITY_POLYNOMIAL:
	poly = c->vitality_polynomial;
	p = reactor_polynomial_new_from_poly (&poly);
	g_value_take_boxed (value, p);
	break;

    case PROP_CAPACITY_LIMIT_MIN:
	g_value_set_int (value, c->capacity_limit_min);
	break;

    case PROP_CAPACITY_LIMIT_MAX:
	g_value_set_int (value, c->capacity_limit_max);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

/**
 * reactor_config_new:
 *
 * Creates default #ReactorConfig instance.
 *
 * returns: (transfer full): new #ReactorConfig instance with default values
 */
ReactorConfig *
reactor_config_new ()
{
    return reactor_config_new_with_config (NULL);
}

/**
 * reactor_config_new_copy: (constructor)
 * @other: other instance
 *
 * Creates new #ReactorConfig and copies properties from @other.
 *
 * returns: (transfer full): copied #other
 */
ReactorConfig *
reactor_config_new_copy (ReactorConfig *other)
{
    return reactor_config_new_with_config (reactor_config_get_config (other));
}

/**
 * reactor_config_new_with_config: (skip)
 * @config: internal config
 *
 * Creates new config for @config.
 *
 * returns: (transfer full): new #ReactorConfig for @config
 */
ReactorConfig *
reactor_config_new_with_config (const struct rcom_conf *config)
{
    return g_object_new (REACTOR_TYPE_CONFIG,
			 "config", config,
			 NULL);
}

/**
 * reactor_config_get_config: (skip)
 * @self: a #ReactorConfig
 *
 * Gets internal instance.
 *
 * returns: (transfer none): internal config instance
 */
struct rcom_conf *
reactor_config_get_config (ReactorConfig *self)
{
    if (self->config == NULL) {
	self->config = g_new0 (struct rcom_conf, 1);

	g_object_notify (G_OBJECT (self), "config");
    }

    return self->config;
}
