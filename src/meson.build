# Copyright (c) 2020 Reactor Energy
# Mieszko Mazurek <mimaz@gmx.com>


header_subdir = 'reactor-energy'
with_tty = host_machine.system() == 'linux'
enums = gnome.mkenums_simple('reactor-types-gtype',
  sources: 'reactor-types.h',
  install_header: true,
  install_dir: join_paths('include', header_subdir))

config_data = configuration_data()
config_data.set('with_tty', with_tty.to_int())
config_data.set('with_gio', gio_dep.found().to_int())
config = configure_file(input: 'reactor-gobject-config.h.in',
  output: 'reactor-gobject-config.h',
  configuration: config_data)

export_data = configuration_data()
export_data.set('ld_library_path', meson.current_build_dir())
export_data.set('gi_typelib_path', meson.current_build_dir())
export = configure_file(input: 'export-paths.sh.in',
  output: 'export-paths.sh',
  configuration: export_data)

sources = [
  'reactor-config.c',
  'reactor-com.c',
  'reactor-com-serial.c',
  'reactor-com-tty.c',
  'reactor-com-dummy.c',
  'reactor-polynomial.c',
  'reactor-types.c',
  enums,
]

headers = [
  'reactor-config.h',
  'reactor-com.h',
  'reactor-com-serial.h',
  'reactor-com-tty.h',
  'reactor-com-dummy.h',
  'reactor-polynomial.h',
  'reactor-gobject.h',
  'reactor-types.h',
  config,
]

gir_sources = [
  sources,
  headers,
  enums,
]

reactor_gobject_lib = library(meson.project_name(),
  sources,
  dependencies: [
    reactor_com_dep,
    gobject_dep,
    gio_dep,
  ],
  install: true)

reactor_gobject_src_dir = meson.current_source_dir()
reactor_gobject_inc = include_directories('.')

if with_gir
  reactor_gobject_gir = gnome.generate_gir(reactor_gobject_lib,
    sources: gir_sources,
    namespace: 'Reactor',
    nsversion: meson.project_version(),
    header: 'reactor-gobject.h',
    includes: ['GObject-2.0'],
    install: true)
endif

if with_vala
  reactor_gobject_vapi = gnome.generate_vapi(meson.project_name(),
    sources: reactor_gobject_gir.get(0),
    packages: 'gobject-2.0',
    install: true)
endif

reactor_gobject_dep = declare_dependency(link_with: reactor_gobject_lib,
  dependencies: [
    reactor_com_dep,
    gobject_dep,
  ],
  sources: enums.get(1),
  include_directories: reactor_gobject_inc)

install_headers(headers, subdir: header_subdir)

pkg_mod = import('pkgconfig')
pkg_mod.generate(
    libraries: reactor_gobject_lib,
    version: meson.project_version(),
    name: 'reactor-gobject',
    subdirs: header_subdir,
    description: 'The library that facilitates the communication ' +
		 'and protocol between a Reactor battery and a desktop client'
)

if with_vala
  meson.override_dependency('reactor-gobject',
    declare_dependency(dependencies: [
      reactor_gobject_dep,
      reactor_gobject_vapi,
    ]))
else
  meson.override_dependency('reactor-gobject', reactor_gobject_dep)
endif
