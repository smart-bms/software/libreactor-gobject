/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/**
 * SECTION:reactor-com-dummy
 * @short_description: dummy implementation of #ReactorCom
 * @title:ReactorComDummy
 *
 * #ReactorComDummy implements dummy implementation of the protocol with the
 * purpose of #ReactorCom testing. It stores virtually device's configuration
 * in given device path so it can be preserved between object initializations.
 * All other state is volatile but aims to emulate the real device behaviour.
 */

#include "reactor-com-dummy.h"
#include "reactor-gobject-config.h"

#if __REACTOR_GOBJECT_WITH_GIO
#include <gio/gio.h>
#endif

struct dummy_com
{
    struct rcom com;
    ReactorComDummy *self;
};

typedef struct {
    gchar *config_path;

    struct dummy_com trans;
    gint64 time_offset;
    GTimer *timer;
    GRand *rand;
    guint32 balancing;
} ReactorComDummyPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ReactorComDummy, reactor_com_dummy, REACTOR_TYPE_COM);

enum
{
    PROP_0,
    PROP_CONFIG_PATH,
    N_PROPS,
};

static GParamSpec *props[N_PROPS];

static void constructed (GObject *gobj);
static void finalize (GObject *gobj);
static void set_property (GObject *gobj, guint propid,
			  const GValue *value, GParamSpec *spec);
static void get_property (GObject *gobj, guint propid,
			  GValue *value, GParamSpec *spec);

static ReactorComDummy *
get_self (struct rcom *trans)
{
    return ((struct dummy_com *) trans)->self;
}

static ReactorComDummyPrivate *
get_private (struct rcom *trans)
{
    ReactorComDummy *self;

    self = get_self (trans);

    return reactor_com_dummy_get_instance_private (self);
}

static int8_t
trans_ping (struct rcom *com, uint8_t ack)
{
    if (ack > 0) {
	rcom_ping (com->receiver, ack - 1);
    }

    return 0;
}

static int8_t
trans_timestamp (struct rcom *com, int64_t time)
{
    get_private (com)->time_offset = time;

    return 0;
}

static int8_t
trans_message (struct rcom *com, const char *msg)
{
    (void) com;
    g_message ("sending dummy message: %s", msg);

    return 0;
}

static int8_t
trans_config (struct rcom *com, const struct rcom_conf *conf)
{
#if __REACTOR_GOBJECT_WITH_GIO
    g_autoptr (GFile) file = NULL;
    g_autoptr (GFileIOStream) io = NULL;
    g_autoptr (GOutputStream) out = NULL;
    ReactorComDummyPrivate *p;

    p = get_private (com);
    file = g_file_new_for_path (p->config_path);

    if (file == NULL) {
	return -1;
    }

    io = g_file_replace_readwrite (file, NULL,
				   FALSE, G_FILE_CREATE_NONE,
				   NULL, NULL);

    if (io == NULL) {
	return -1;
    }

    out = g_io_stream_get_output_stream (G_IO_STREAM (io));

    if (g_output_stream_write_all (out, conf, sizeof (struct rcom_conf),
				   NULL, NULL, NULL)) {
	g_message ("config written");
	return 0;
    }

    return -1;
#else
    (void) com;
    (void) conf;

    g_warning ("cannot write config because of missing GIO");
    return -1;
#endif
}

static int8_t
trans_caplimit (struct rcom *com, uint8_t limit)
{
    (void) com;
    (void) limit;

    return 0;
}

static void
send_voltages (struct rcom *com)
{
    ReactorComDummyPrivate *p;
    gint16 values[10];
    guint i;

    p = get_private (com);

    for (i = 0; i < G_N_ELEMENTS (values); i++) {
	values[i] = g_rand_int_range (p->rand, 3680, 3720);
    }

    rcom_voltage (com, values, G_N_ELEMENTS (values));
}

static gboolean
load_config (ReactorComDummy *self, struct rcom_conf *conf)
{
    g_autoptr (GFile) file = NULL;
    g_autoptr (GFileInputStream) is = NULL;
    ReactorComDummyPrivate *p;
    gssize readno;

    p = reactor_com_dummy_get_instance_private (self);
    file = g_file_new_for_path (p->config_path);

    g_return_val_if_fail (file != NULL, FALSE);

    is = g_file_read (file, NULL, NULL);
    readno = g_input_stream_read (G_INPUT_STREAM (is), conf,
				  sizeof (struct rcom_conf),
				  NULL, NULL);

    return readno == sizeof (struct rcom_conf);
}

static int8_t
trans_query (struct rcom *com, uint32_t query)
{
    ReactorComDummyPrivate *p;
    struct rcom_conf conf;

    p = get_private (com);

    if (query & REACTOR_COM_QUERY_VOLTAGE) {
	send_voltages (com->receiver);
    }

    if (query & REACTOR_COM_QUERY_TIMESTAMP) {
	rcom_timestamp (com->receiver, get_private (com)->time_offset);
    }

    if (query & REACTOR_COM_QUERY_CURRENT) {
	rcom_current (com->receiver,
		      g_rand_int_range (p->rand, 1000, 1200));
    }

    if (query & REACTOR_COM_QUERY_STATUS) {
	rcom_status (com->receiver,
		     REACTOR_COM_STATUS_CHARGING |
		     REACTOR_COM_STATUS_DISCHARGING);
    }

    if (query & REACTOR_COM_QUERY_PROTECTION) {
	rcom_protection (com->receiver, 0);
    }

    if (query & REACTOR_COM_QUERY_BALANCING) {
	rcom_balancing (com->receiver, p->balancing);
    }

    if (query & REACTOR_COM_QUERY_CONFIG) {
	if (load_config (get_self (com), &conf)) {
	    rcom_config (com->receiver, &conf);
	    g_message ("config queried");
	} else {
	    g_warning ("loading config from failed");
	}
    }

    return 0;
}

static struct rcom_api trans_api = {
    .ping = trans_ping,
    .timestamp = trans_timestamp,
    .message = trans_message,
    .config = trans_config,
    .caplimit = trans_caplimit,
    .query = trans_query,
};

static void
reactor_com_dummy_init (ReactorComDummy *self)
{
    (void) self;
}

static void
reactor_com_dummy_class_init (ReactorComDummyClass *cls)
{
    GObjectClass *gcls = G_OBJECT_CLASS (cls);

    gcls->constructed = constructed;
    gcls->finalize = finalize;
    gcls->set_property = set_property;
    gcls->get_property = get_property;

    props[PROP_CONFIG_PATH] =
	g_param_spec_string ("config-path",
			     "config path",
			     "path to file where store binary config object",
			     "/tmp/reactor-config.bin",
			     G_PARAM_READWRITE |
			     G_PARAM_CONSTRUCT_ONLY |
			     G_PARAM_STATIC_STRINGS);

    g_object_class_install_properties (gcls, N_PROPS, props);
}

static void
constructed (GObject *gobj)
{
    ReactorComDummy *self;
    ReactorComDummyPrivate *p;
    struct rcom *recv;

    G_OBJECT_CLASS (reactor_com_dummy_parent_class)->constructed (gobj);

    self = REACTOR_COM_DUMMY (gobj);
    p = reactor_com_dummy_get_instance_private (self);

    p->timer = g_timer_new ();
    p->rand = g_rand_new ();

    g_object_get (self, "receiver", &recv, NULL);

    rcom_init ((struct rcom *) &p->trans, &trans_api, recv);
    p->trans.self = self;

    g_object_set (self, "transmitter", &p->trans, NULL);
}

static void
finalize (GObject *gobj)
{
    ReactorComDummy *self;
    ReactorComDummyPrivate *p;

    self = REACTOR_COM_DUMMY (gobj);
    p = reactor_com_dummy_get_instance_private (self);

    g_timer_destroy (p->timer);
    g_rand_free (p->rand);

    G_OBJECT_CLASS (reactor_com_dummy_parent_class)->finalize (gobj);
}

static void
set_property (GObject *gobj, guint propid,
	      const GValue *value, GParamSpec *spec)
{
    ReactorComDummy *self;
    ReactorComDummyPrivate *p;

    self = REACTOR_COM_DUMMY (gobj);
    p = reactor_com_dummy_get_instance_private (self);

    switch (propid) {
    case PROP_CONFIG_PATH:
	g_clear_pointer (&p->config_path, g_free);
	p->config_path = g_value_dup_string (value);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

static void
get_property (GObject *gobj, guint propid,
	      GValue *value, GParamSpec *spec)
{
    ReactorComDummy *self;
    ReactorComDummyPrivate *p;

    self = REACTOR_COM_DUMMY (gobj);
    p = reactor_com_dummy_get_instance_private (self);

    switch (propid) {
    case PROP_CONFIG_PATH:
	g_value_set_string (value, p->config_path);
	break;

    default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (gobj, propid, spec);
    }
}

/**
 * reactor_com_dummy_new:
 * @config_path: config path
 *
 * Creates #ReactorComDummy for given config path.
 *
 * returns: new #ReactorComDummy
 */
ReactorCom *
reactor_com_dummy_new (const gchar *config_path)
{
    return g_object_new (REACTOR_TYPE_COM_DUMMY,
			 "config-path", config_path,
			 NULL);
}
