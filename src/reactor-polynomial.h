/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

#pragma once

#include <glib-object.h>
#include <rcom-poly.h>

#define REACTOR_TYPE_POLYNOMIAL (reactor_polynomial_get_type ())

typedef struct _ReactorPolynomial ReactorPolynomial;

struct _ReactorPolynomial
{
    gfloat coeffs[RCOM_POLY_DEGREE + 1];
};

GType reactor_polynomial_get_type () G_GNUC_UNUSED;
ReactorPolynomial *reactor_polynomial_new ();
ReactorPolynomial *reactor_polynomial_new_from_poly (struct rcom_poly *poly);
ReactorPolynomial *reactor_polynomial_new_from_array (const gfloat *coeffs,
						      gint count);
ReactorPolynomial *reactor_polynomial_copy (ReactorPolynomial *self);
void reactor_polynomial_free (ReactorPolynomial *self);
void reactor_polynomial_init_poly (ReactorPolynomial *self,
				   struct rcom_poly *poly);
guint32 reactor_polynomial_compute (ReactorPolynomial *self,
				    guint32 value);
