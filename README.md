# Reactor common library

A GObject wrapper providing an easy to use API around [libreactor-gobject][1]
to build applications that interface with a Reactor battery.

## Building, Testing, and Installation

This library downloads and uses [libreactor-com][1] to build.

Also `meson` (and, by extension, `ninja`) is a requirement to build the
library, since it's the building system used in this project.

To build the library:

	meson build
	cd build
	ninja

Optionaly, if you also want to build the project's documentation, add the docs
option to meson before running ninja:

	meson build -Ddoc=true

To install the application, run ninja with the "install" argument:

	ninja install

And to uninstall, run ninja with the uninstall argument and elevated permissions

	sudo ninja uninstall

## Documentation

You'll find useful reference to the library's C API in the project's [pages][2].
Although this documentation is only for C, the library can be used in all
langauges that support GObject, including Python, Java, C++, D, Rust, Go, Vala, 
and many more. The library is made available to these languages by using 
GObject's introspection tools, so the API in these languages should be 
consistent enoughfor the C documentation to be useful for those languages too.

<!--TODO: Add Debugging functionality and options-->

[1]: https://gitlab.com/smart-bms/software/libreactor-com/-/tree/master
[2]: https://smart-bms.gitlab.io/software/libreactor-gobject/
