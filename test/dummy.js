#!/usr/bin/gjs
/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

imports.gi.versions.Reactor = '0.1';

const Reactor = imports.gi.Reactor;

let com = new Reactor.ComDummy ();

com.connect ("receive-voltage", (com, values) => {
    print ("received " + values.length + " voltages:");

    for (var voltage of values) {
	print (voltage);
    }
});

com.connect ("receive-ping", (com, ack) => {
    print ("pinged back " + ack);

    if (ack > 0) {
	com.send_ping (ack - 1);
    }
});

com.connect ("receive-current", (com, value) => {
    print ("received current " + value);
});

com.connect ("receive-status", (com, flags) => {
    print ("received status: " + flags);
});

com.connect ("receive-protection", (com, flags) => {
    print ("received protection: " + flags);
});

com.connect ("receive-balancing", (com, flags) => {
    print ("received balancing: " + flags);
});

com.send_ping (10);
com.send_message ("hello, dummy reactor");
com.send_query (Reactor.ComQuery.VOLTAGE |
		Reactor.ComQuery.CURRENT |
		Reactor.ComQuery.STATUS |
		Reactor.ComQuery.PROTECTION |
		Reactor.ComQuery.BALANCING);
